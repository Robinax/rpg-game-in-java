package Exceptions;

public class InvalidArmorException extends Exception{
    //Exception for invalid armor expressions
    public InvalidArmorException(String errorMessage){
        super(errorMessage);
    }
}
