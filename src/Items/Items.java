package Items;

import Utils.Slots;

public abstract class Items {
    private String name;
    private int levelRequirement;
    private Slots equipSlot;
    private int[] statIncrease = {0,0,0};


    public Items( String name, int levelRequirement, Slots equipSlot ,int[] statIncrease) {

        this.name = name;
        this.levelRequirement = levelRequirement;
        this.equipSlot = equipSlot;
        this.statIncrease = statIncrease;
    }
    //Getters for items. Armors/Weapons
    public int getStatIncrease(int stat) {
        return statIncrease[stat];
    }

    public String getName() {
        return name;
    }


    public int getLevelRequirement() {
        return levelRequirement;
    }

    public Slots getEquipSlot() {
        return equipSlot;
    }


}
