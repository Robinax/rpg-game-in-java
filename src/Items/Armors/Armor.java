package Items.Armors;

import Items.Items;
import Utils.ArmorTypes;
import Utils.Slots;

public class Armor extends Items {

    private ArmorTypes type;


    public Armor(String name, int levelRequirement, Slots equipSlot , ArmorTypes type, int[] statIncrease) {
        super( name, levelRequirement, equipSlot,statIncrease);
        this.type = type;
    }
    //Getter for armor types
    public ArmorTypes getType() {
        return type;
    }
}
