package Items.Weapons;

import Hero.Hero;
import Items.Items;
import Utils.Slots;
import Utils.WeaponTypes;

public class Weapon extends Items {

    private WeaponTypes type;
    private double damage;
    private double attackSpeed;


    public Weapon(String name, int levelRequirement, Slots equipSlot, WeaponTypes type, double damage, double attackspeed, int[] statIncrease) {
        super(name, levelRequirement, equipSlot,statIncrease);
        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackspeed;
    }
    //getters for the weapons created
    public double getDmg() {
        return damage;
    }

    public WeaponTypes getType() {
        return type;
    }

    public double getDamage(Hero hero) {
            return damage += hero.getPrimaryAttribute() * 0.10;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public double getWeaponDps( ){
       return this.damage * this.attackSpeed;
    }




}
