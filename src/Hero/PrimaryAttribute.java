package Hero;

public class PrimaryAttribute {
    private int[] stat = {0,0,0};
    //Class for attributes
    public PrimaryAttribute(int strStat, int dexStat, int intStat) {
        this.stat[0] = strStat;
        this.stat[1] = dexStat;
        this.stat[2] = intStat;
    }
    public int getPrimaryAttributeStr() {
        return this.stat[0];
    }
    public int getPrimaryAttributeDex() {
        return this.stat[1];
    }
    public int getPrimaryAttributeInt() {
        return this.stat[2];
    }



}
