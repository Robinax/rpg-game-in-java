package Hero;

import Items.Armors.Armor;
import Items.Weapons.Weapon;
import Utils.Slots;

public interface CharacterFunction {
//Functions that heros/characters have to have.
    public void levelUp();
    boolean equipWeapon(Weapon weapon) throws Exception;
    boolean equipArmor(Armor armor) throws Exception;

    boolean unequipItem(Slots slot);

    public void getDetails();
}
