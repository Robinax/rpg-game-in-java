package Hero.Classes;

import Hero.Hero;
import Hero.PrimaryAttribute;
import Utils.ArmorTypes;
import Utils.PrimaryStat;
import Utils.WeaponTypes;

public class Ranger extends Hero {

    public Ranger(String name) {
        super(name,1, 1, 7, 1, new ArmorTypes[]{ArmorTypes.LEATHER,ArmorTypes.MAIL,ArmorTypes.NONE},new WeaponTypes[]{WeaponTypes.BOW,WeaponTypes.NONE}
        , PrimaryStat.DEX);
    }
    //Level up function for the classes. The all got different stats when they level up so its in their
    //Classes we can set it to what we want.
    public void levelUp(){
        this.setAttribute(new PrimaryAttribute(1,5,1));
        this.level ++;
    }
}
