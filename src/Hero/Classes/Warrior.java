package Hero.Classes;

import Hero.Hero;
import Hero.PrimaryAttribute;
import Utils.ArmorTypes;
import Utils.PrimaryStat;
import Utils.WeaponTypes;

public class Warrior extends Hero {

    public Warrior(String name) {
        super(name,1, 5, 2, 1,
                new ArmorTypes[]{ArmorTypes.MAIL, ArmorTypes.PLATE,ArmorTypes.NONE}, new WeaponTypes[]{WeaponTypes.SWORD,WeaponTypes.AXE,WeaponTypes.HAMMER,WeaponTypes.NONE},
                PrimaryStat.STR);
    }

    //Level up function for the classes. The all got different stats when they level up so its in their
    //Classes we can set it to what we want.
    public void levelUp(){

        this.setAttribute(new PrimaryAttribute(3,2,0));
        this.level ++;
    }

}
