package Hero.Classes;

import Hero.Hero;
import Hero.PrimaryAttribute;
import Utils.ArmorTypes;
import Utils.PrimaryStat;
import Utils.WeaponTypes;

public class Rogue extends Hero {

    public Rogue(String name) {
        super(name, 1, 2, 6, 1, new ArmorTypes[]{ArmorTypes.CLOTH,ArmorTypes.LEATHER,ArmorTypes.NONE},new WeaponTypes[] {WeaponTypes.DAGGER,WeaponTypes.SWORD,WeaponTypes.NONE}
        , PrimaryStat.DEX);
    }
    //Level up function for the classes. The all got different stats when they level up so its in their
    //Classes we can set it to what we want.
    public void levelUp(){

        this.setAttribute(new PrimaryAttribute(1,4,1));
        this.level ++;
    }

}
